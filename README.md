### What is this repository for? ###

This project provides a mobile application counterpart to a golf tournament registration web application. 
 
This project provides a mobile application which will allow various functions to be performed related to golf tournament registration.  The various functions to be provided will be outlined in the primary functions section of this document.

##Primary Functions##
 * Welcome Page 
 * Material Design with Custom Theme
 * Navigation Drawer
 * Search Dialog
 * Menu
 * Social Media Sharing
 * Web Services
 * Dynamic Layout
 * Responsive UI
 * Local Calendar Update
 * Local SQLite database
 * Splash Screen
 * Custom Icon 
 
### How do I get set up? ###

* Pull Repository using SourceTree or Git
* Open Project using Android Studio 2.2.3+
* Use default emulator if using Intel CPU, or configure GenyMotion if using AMD CPU.
* Run project on emulator.

### Who do I talk to? ###

* Kyle Creamer