package com.example.kcreamer.golftournaments;


import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class TournamentDataSource {

    // Database fields
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;
    private String[] tournamentColumns = { SQLiteHelper.COLUMN_TOURNAMENT_ID,
            SQLiteHelper.COLUMN_NAME, SQLiteHelper.COLUMN_STARTDATE, SQLiteHelper.COLUMN_ENDDATE, SQLiteHelper.COLUMN_ADDRESS};
    private String[] playerColumns = { SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_PLAYER_ID, SQLiteHelper.COLUMN_PLAYER_NAME, SQLiteHelper.COLUMN_PLAYER_TEAM, SQLiteHelper.COLUMN_PLAYER_EMAIL,
    SQLiteHelper.COLUMN_PLAYER_TOURNAMENT};

    public TournamentDataSource(Context context) {
        dbHelper = new SQLiteHelper(context);
    }




    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Tournament createTournament(Tournament tournament) {
        Tournament newTournament = new Tournament();
        Long insertId;
        Cursor cursor;
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_TOURNAMENT_ID, tournament.getId());
        values.put(SQLiteHelper.COLUMN_NAME, tournament.getName());
        values.put(SQLiteHelper.COLUMN_STARTDATE, tournament.getStartDate());
        values.put(SQLiteHelper.COLUMN_ENDDATE, tournament.getEndDate());
        values.put(SQLiteHelper.COLUMN_ADDRESS, tournament.getAddress());
        open();
        try {
            String sql = "SELECT * FROM " + SQLiteHelper.TABLE_TOURNAMENT_ENTRIES +" WHERE " +SQLiteHelper.COLUMN_TOURNAMENT_ID + "= ?";
            String searchValue = Integer.toString(tournament.getId());
            cursor = database.rawQuery(sql, new String[]{searchValue});
            if (cursor == null || !cursor.moveToFirst()) {
                //Insert new
                insertId = database.insert(SQLiteHelper.TABLE_TOURNAMENT_ENTRIES, null,
                        values);
            } else {
                //Update
                String clause = "_id = ?";
                String args[] = {searchValue};
                database.update(SQLiteHelper.TABLE_TOURNAMENT_ENTRIES, values, clause, args);

            }
            cursor.close();
        }
        catch(SQLiteConstraintException e)
        {
            Log.i("Exception: ", e.toString());
        }
            close();
        return newTournament;
        }


    public Player createPlayer(Player player) {
        Long insertId;
        Cursor cursor;


        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_PLAYER_ID, player.getPlayerId());
        values.put(SQLiteHelper.COLUMN_PLAYER_NAME, player.getPlayerName());
        values.put(SQLiteHelper.COLUMN_PLAYER_TEAM, player.getPlayerTeam());
        values.put(SQLiteHelper.COLUMN_PLAYER_EMAIL, player.getPlayerEmail());
        values.put(SQLiteHelper.COLUMN_PLAYER_TOURNAMENT, player.getTournamentID());
        open();
        try {
            String sql = "SELECT * FROM " + SQLiteHelper.TABLE_PLAYERS + " WHERE " + SQLiteHelper.COLUMN_PLAYER_ID + "= ? AND " + SQLiteHelper.COLUMN_PLAYER_TOURNAMENT + "= ?";
            String searchValue = player.getPlayerId();
            cursor = database.rawQuery(sql, new String[]{searchValue, player.getTournamentID()});
            if (cursor == null || !cursor.moveToFirst()) {
                //Insert new
                insertId = database.insert(SQLiteHelper.TABLE_PLAYERS, null,
                        values);
            } else {
                //Update
                String clause = "playerId= ? AND playerTournament= ?";
                String args[] = {searchValue, player.getTournamentID()};
                database.update(SQLiteHelper.TABLE_PLAYERS, values, clause, args);

            }
            cursor.close();
        }
            catch(SQLiteConstraintException e)
            {
                Log.i("Exception: ", e.toString());
            }
        close();
        return player;
    }


    public List<Tournament> getAllTournaments() {
        List<Tournament> entries = new ArrayList<Tournament>();

        open();
        Cursor cursor = database.query(SQLiteHelper.TABLE_TOURNAMENT_ENTRIES,
                tournamentColumns, null, null, null, null, null);


        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Tournament tournament = cursorToTournament(cursor);
            entries.add(tournament);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        close();
        return entries;
    }

    public ArrayList<Player> getPlayersForTournament(String searchValue) {
        ArrayList<Player> entries = new ArrayList<>();

        String whereClause = "playerTournament= ?";
        String[] whereArgs = new String[] {
                searchValue
        };

        Cursor cursor = database.query(SQLiteHelper.TABLE_PLAYERS, playerColumns, whereClause, whereArgs,
                null, null, null);


        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Player player = cursorToPlayer(cursor);
            entries.add(player);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();

        return entries;
    }

    private Tournament cursorToTournament(Cursor cursor) {
        Tournament tournament = new Tournament();
        tournament.setId(cursor.getInt(0));
        tournament.setName(cursor.getString(1));
        tournament.setStartDate(cursor.getString(2));
        tournament.setEndDate(cursor.getString(3));
        tournament.setAddress(cursor.getString(4));
        return tournament;
    }

    private Player cursorToPlayer(Cursor cursor) {
        Player player = new Player();
        player.setId(0);
        player.setPlayerId(cursor.getString(1));
        player.setPlayerName(cursor.getString(2));
        player.setPlayerTeam(cursor.getString(3));
        player.setPlayerEmail(cursor.getString(4));
        player.setTournamentID(cursor.getString(5));
        return player;
    }


    public ArrayList<Tournament> getlist(String search) {

        ArrayList<Tournament> entries = new ArrayList<>();

        try {

            String whereClause = "name LIKE ?";
            String[] whereArgs = new String[] {
                  "%" + search + "%"
            };
            open();
            Cursor cursor = database.query(SQLiteHelper.TABLE_TOURNAMENT_ENTRIES, tournamentColumns, whereClause, whereArgs,
                    null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Tournament tournament = cursorToTournament(cursor);
                    entries.add(tournament);
                    cursor.moveToNext();
                }
            }
            close();
            return entries;
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        return entries;
    }
}