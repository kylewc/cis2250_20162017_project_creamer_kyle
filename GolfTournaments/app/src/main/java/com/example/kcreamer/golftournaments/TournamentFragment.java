package com.example.kcreamer.golftournaments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;



/**
 * Created by shina on 2017-01-18.
 */

public class TournamentFragment extends Fragment {

    View myView;
    private TournamentDataSource datasource = new TournamentDataSource(getActivity());
    TournamentListFragment tournamentlist;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.tournaments, container, false);
        super.onCreate(savedInstanceState);

        FragmentManager childFragMan = getFragmentManager();

        FragmentTransaction childFragTrans = childFragMan.beginTransaction();
        tournamentlist = new TournamentListFragment();
        childFragTrans.replace(R.id.tournamentsFragment, tournamentlist);

        childFragTrans.commit();

        return myView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onResume() {
        datasource = new TournamentDataSource(getActivity());
        datasource.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        datasource.close();
        super.onPause();
    }

}



