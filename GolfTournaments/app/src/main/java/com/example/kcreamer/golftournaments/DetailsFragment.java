package com.example.kcreamer.golftournaments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Environment;
import android.provider.CalendarContract;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * Created by shina on 2017-01-18.
 */

public class DetailsFragment extends Fragment implements View.OnClickListener{

    private TournamentDataSource datasource = new TournamentDataSource(getActivity());
    private TextView tournamentName;
    private TextView tournamentAddress;
    private TextView startDate;
    private TextView endDate;
    private ListView playerList= null;
    private Tournament tournament;
    private ArrayList<Player> players;
    private PlayerAdapter adapter;
    private View myView;
    private String searchValue;


    public static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR =  1;
    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

                myView = inflater.inflate(R.layout.fragment_details, container, false);
                setRetainInstance(true);


                tournament = getTournament();
                datasource = new TournamentDataSource(this.getActivity());
                datasource.open();
                searchValue = Integer.toString(tournament.getId());

                players = datasource.getPlayersForTournament(searchValue);

                Log.i("DETAILS FRAGMENT: ", "I am in the details fragment.");
                tournamentName = (TextView) myView.findViewById(R.id.tournamentName);
                tournamentAddress = (TextView) myView.findViewById(R.id.address);
                startDate = (TextView) myView.findViewById(R.id.startDate);
                endDate = (TextView) myView.findViewById(R.id.endDate);
                Button calendarButton = (Button) myView.findViewById(R.id.calendarButton);
                calendarButton.setOnClickListener(this);

                tournamentName.setText(tournament.getName());
                tournamentAddress.setText(tournament.getAddress());
                startDate.setText(tournament.getStartDate());
                endDate.setText(tournament.getEndDate());

                playerList = (ListView) myView.findViewById(R.id.playerList);
                adapter = new PlayerAdapter(getActivity(), players);
                playerList.setAdapter(adapter);

        ImageButton facebook = (ImageButton) myView.findViewById(R.id.facebook);
        facebook.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bitmap shareImg = takeScreenshot();

                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(shareImg)
                        .setCaption("Upcoming Golf Tournament")
                        .build();

                SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();

                ShareApi.share(content, null);
                ShareDialog.show(getActivity(), content);

            }
        });

        ImageButton twitter = (ImageButton) myView.findViewById(R.id.twitter);
        twitter.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String url = "http://www.twitter.com/intent/tweet?text="+tournament.toString();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                }
    });


        ImageButton email = (ImageButton) myView.findViewById(R.id.email);
        email.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Golf Tournament");
                emailIntent.putExtra(Intent.EXTRA_TEXT, tournament.toString());
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });


        return myView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void onClick(View v) {

        if (v.getContext().checkSelfPermission(Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            Log.i("Permissions", "There is a permissions issue");

            this.requestPermissions(new String[]{Manifest.permission.WRITE_CALENDAR}, MY_PERMISSIONS_REQUEST_WRITE_CALENDAR);
            ;

        }

        else{

            writeCalendarEvent(myView);
        }
    }

        public void writeCalendarEvent(View myView){
            long startMillis = 0;
            long endMillis = 0;

            Log.i("Writing Calendar", "I am in the write calendar event");

            String[] theDate = (startDate.getText().toString()).split("-");
            int year = Integer.parseInt(theDate[0]);
            int day = Integer.parseInt(theDate[2]);
            int month = Integer.parseInt(theDate[1]) - 1;
            Log.d("info", "The date is" + year + "-" + month + "-" + day);
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(year, month, day, 0, 0);
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(year, month, day);
            endMillis = endTime.getTimeInMillis();
            ContentResolver cr = myView.getContext().getContentResolver();
            ContentValues values = new ContentValues();

            TimeZone timeZone = TimeZone.getDefault();
            Log.d("info", "The default time zone is  " + timeZone);
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
            values.put(CalendarContract.Events.TITLE, tournamentName.getText().toString());
            values.put(CalendarContract.Events.DESCRIPTION, tournamentAddress.getText().toString());
            values.put(CalendarContract.Events.CALENDAR_ID, 1);
            values.put(CalendarContract.Events.ALL_DAY, 1);
            values.put(CalendarContract.Events.HAS_ALARM, 1);

            if (myView.getContext().checkSelfPermission(Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

                    Log.i("Permissions", "There was a permissions issue");

            }

            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

            Log.i("Calendar", "I added the event");
        // Retrieve ID for new event
        String eventID = uri.getLastPathSegment();
        Toast.makeText(myView.getContext(), "New event inserted", Toast.LENGTH_LONG)
                .show();

    }

    @Override
    public void onResume() {
        datasource = new TournamentDataSource(getActivity());
        datasource.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        datasource.close();
        super.onPause();
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    {
                        writeCalendarEvent(myView);

                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }


                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request

        }
    }

    private Bitmap takeScreenshot() {

        // create bitmap screen capture
        View v1 = getActivity().getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        return v1.getDrawingCache();
    }

}



