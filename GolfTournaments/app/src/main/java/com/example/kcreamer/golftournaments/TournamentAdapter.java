package com.example.kcreamer.golftournaments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kcreamer on 1/20/2017.
 */

public class TournamentAdapter extends ArrayAdapter<Tournament> {
    static class ViewHolder {
        TextView name;
        TextView date;
        TextView address;
    }

    private  Context context;
    private  List<Tournament> values;

    public TournamentAdapter(Context context, List<Tournament> values) {
        super(context, R.layout.list_row, values);
        this.context = context;
        this.values = values;
    }

    public void updateTournaments(List<Tournament> newlist) {
        values.clear();
        values.addAll(newlist);
        this.notifyDataSetChanged();
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();


        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.list_row, null);


            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            viewHolder.address = (TextView) convertView.findViewById(R.id.address);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Tournament item = getItem(position);
        if (item!= null) {
            viewHolder.name.setText(String.format(item.getName()));
            viewHolder.date.setText(String.format(item.getStartDate()));
            viewHolder.address.setText(String.format( item.getAddress()));
        }

        return convertView;
    }

    public void update () {
        notifyDataSetChanged();
    }
}