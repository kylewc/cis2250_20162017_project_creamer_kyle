package com.example.kcreamer.golftournaments;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;

import android.content.res.Configuration;
import android.os.Bundle;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.ListView;

import java.util.List;


/**
 * Created by shina on 2017-01-18.
 */

public class TournamentListFragment extends ListFragment{

    View view;
    private TournamentDataSource datasource;
    private ArrayAdapter<Tournament> adapter;
    private List<Tournament> values;
    private DetailsFragment detailsFragment = new DetailsFragment();
    private Tournament thisTournament;



    public ArrayAdapter<Tournament> getAdapter() {
        return adapter;
    }

    public List<Tournament> getValues() {
        return values;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.tournaments, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        FragmentManager fm = getFragmentManager();
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            Log.i("ActivityCreated", "My saved instance state is null");
            datasource = new TournamentDataSource(this.getActivity());

            datasource.open();

            values = datasource.getAllTournaments();

            adapter = new TournamentAdapter(this.getActivity(), values);
            setListAdapter(adapter);
            setRetainInstance(true);
        }
        else{

            if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
                //This works on emulator but not Device.
                DetailsFragment detailsFragmentCheck = (DetailsFragment) fm.findFragmentByTag("DETAILS_FRAGMENT_LAND");
                if (detailsFragmentCheck != null)
                {
                    //This is an awful solution but works without going much deeper into dynamic layouts and child fragment management (a non-six week android course).\
                    getListView().setVisibility(View.INVISIBLE);


                    Log.i("ASDF", "Managing orientation from landscape to portrait");

                    fm.popBackStack();
                    fm.beginTransaction().remove(detailsFragmentCheck).commit();
                    fm.executePendingTransactions();
                    //New instance works on 7.0 emulator but not marshmallow
//                    detailsFragment = new DetailsFragment();
//                    detailsFragment.setTournament(detailsFragmentCheck.getTournament());
                    fm.beginTransaction()
                            .replace(R.id.content_frame, detailsFragmentCheck, "DETAILS_FRAGMENT_LAND").addToBackStack("DETAILS_FRAGMENT_PORT")
                            .commit();
                }
            }
        }
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //here we will need to load the details fragment.


        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        thisTournament = values.get(position);
        Log.i("Values List Item Click:", thisTournament.toString());
        DetailsFragment detailsFragment = new DetailsFragment();
        detailsFragment.setTournament(thisTournament);
        Log.i("View", "On list item click sent list view: " + l.toString() + " view: " + v.toString());
        if(Configuration.ORIENTATION_PORTRAIT==getResources().getConfiguration().orientation) {
            Log.i("Portrait", "I am portrait.");
            fragmentTransaction.replace(R.id.content_frame, detailsFragment, "DETAILS_FRAGMENT_PORT").addToBackStack("DETAILS_FRAGMENT_PORT");
            fragmentTransaction.commit();
        }
        else{
            Log.i("Landscape", "I am landscape.");
            fragmentTransaction.replace(R.id.frameDetails, detailsFragment, "DETAILS_FRAGMENT_LAND").addToBackStack("DETAILS_FRAGMENT_LAND");
            fragmentTransaction.commit();
        }
            thisTournament=null;

        Log.i("FragmentList", "Item clicked: " + id);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml

    //Need to add code to display details of item in separate fragment.

    @Override
    public void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        datasource.open();
        super.onPause();
    }



}



