package com.example.kcreamer.golftournaments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by bjmaclean on 1/24/2017.
 */

public class ConnectionUtil {

    /*
 * Checks if internet is connected.
 *
 * http://developer.android.com/training/monitoring-device-state/connectivity-monitoring.html
 */


    public static boolean isConnected(Context context, String service) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            Connection newConnection = new Connection(service);
            newConnection.execute();
            try {
                String results = newConnection.get();
                if (!results.isEmpty()) {
                    isConnected = true;
                } else {
                    isConnected = false;
                }
            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
        return isConnected;
    }

    public static void getTournaments(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        String results = "";
        if (isConnected) {
            Connection newConnection = new Connection("http://hccis.info:8080/golftournament/api/tournaments/");
//            Connection newConnection = new Connection("http://hccis.info:8080/golftournament/api/tournaments/players/2");

            newConnection.execute();

            //here we parse the tournaments into the database.
            try {
                results = newConnection.get();

                TournamentDataSource src = new TournamentDataSource(context);

                src.open();
                try {
                    JSONArray jarray = new JSONArray(results);


                for (int i = 0; i < jarray.length(); i++) {

                    JSONObject object = jarray.getJSONObject(i);
                    Tournament tournament = new Tournament();
                    tournament.setId(Integer.parseInt(object.getString("tournamentId")));
                    tournament.setName(object.getString("tournamentName"));
                    tournament.setStartDate(object.getString("tournamentStartDate"));
                    tournament.setEndDate(object.getString("tournamentEndDate"));
                    tournament.setAddress(object.getString("tournamentAddress"));
                    src.createTournament(tournament);// Insert record in your DB
                    ConnectionUtil.getPlayersForTournament(context, Integer.toString(tournament.getId()));

                }
                } catch (JSONException e) {
                    Log.d("IOException", e.toString());
                }


                src.close();


            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
    }

    public static void getPlayersForTournament(Context context, String tournamentId) {
        ArrayList<Player> players = new ArrayList<>();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        String results = "";
        if (isConnected) {
            Connection newConnection = new Connection("http://hccis.info:8080/golftournament/api/tournaments/players/" + tournamentId);
//            Connection newConnection = new Connection("http://hccis.info:8080/golftournament/api/tournaments/players/2");

            newConnection.execute();

            //here we parse the tournaments into the database.
            try {
                results = newConnection.get();

                TournamentDataSource src = new TournamentDataSource(context);//Create this object in onCreate() method

                src.open();
                try {
                    JSONArray jarray = new JSONArray(results);

                    Log.i("JSONARRAY : ", jarray.toString());


                    for (int i = 0; i < jarray.length(); i++) {
                        Log.i("OBJECT ON LOOP: ", jarray.getJSONObject(i).toString());
                        JSONObject object = jarray.getJSONObject(i);
                        Player player = new Player();
                        player.setPlayerId(object.getString("playerId"));
                        player.setPlayerName(object.getString("playerName"));
                        player.setPlayerTeam(object.getString("teamId"));
                        player.setPlayerEmail(object.getString("playerEmail"));
                        player.setTournamentID(tournamentId);
                        src.createPlayer(player);// Insert record in your DB
                    }
                } catch (JSONException e) {
                    Log.d("IOException", e.toString());
                }


                src.close();


            } catch (InterruptedException | ExecutionException e) {
                isConnected = false;
            }
        }
    }


}
