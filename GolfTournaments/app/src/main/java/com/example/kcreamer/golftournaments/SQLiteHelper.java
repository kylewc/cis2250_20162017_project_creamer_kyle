package com.example.kcreamer.golftournaments;

/**
 * Created by kcreamer on 1/20/2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

    //Tournament Table
    public static final String TABLE_TOURNAMENT_ENTRIES = "tournament";
    public static final String COLUMN_TOURNAMENT_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_STARTDATE = "startDate";
    public static final String COLUMN_ENDDATE = "endDate";
    public static final String COLUMN_ADDRESS = "address";

    //Player Table
    public static final String TABLE_PLAYERS = "player";
    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_PLAYER_ID = "playerID";
    public static final String COLUMN_PLAYER_NAME = "playerName";
    public static final String COLUMN_PLAYER_TEAM = "playerTeam";
    public static final String COLUMN_PLAYER_EMAIL = "playerEmail";
    public static final String COLUMN_PLAYER_TOURNAMENT = "playerTournament";

    private static final String DATABASE_NAME = "tournament.db";
    private static final int DATABASE_VERSION = 15;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table if not exists "
            + TABLE_TOURNAMENT_ENTRIES + "( " + COLUMN_TOURNAMENT_ID
            + " integer primary key, " + COLUMN_NAME
            + " text not null, " + COLUMN_STARTDATE + " text not null, " +
            COLUMN_ENDDATE + " text not null, " +
            COLUMN_ADDRESS + " text not null);";

    // Table Creation for Players

    private static final String PLAYER_TABLE_CREATE =  "create table if not exists "
            + TABLE_PLAYERS + "( " + COLUMN_ID + " integer primary key, " + COLUMN_PLAYER_ID
            + " int not null, " + COLUMN_PLAYER_NAME
            + " text not null, " + COLUMN_PLAYER_TEAM + " text not null, " +
            COLUMN_PLAYER_EMAIL + " text not null, " + COLUMN_PLAYER_TOURNAMENT + " text not null);";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        database.execSQL(DATABASE_CREATE);
        database.execSQL(PLAYER_TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOURNAMENT_ENTRIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        onCreate(db);
    }

}
