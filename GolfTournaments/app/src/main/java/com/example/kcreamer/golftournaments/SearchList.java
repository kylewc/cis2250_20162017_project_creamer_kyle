package com.example.kcreamer.golftournaments;


import android.app.FragmentTransaction;
import android.app.ListFragment;

import android.app.SearchManager;
import android.os.Bundle;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.ListView;

import java.util.List;


/**
 * Created by shina on 2017-01-18.
 */

public class SearchList extends ListFragment{

    private ArrayAdapter<Tournament> adapter;
    private ListView listView;
    private List<Tournament> values;
    private String query;
    private TournamentDataSource db;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setAdapter(ArrayAdapter<Tournament> adapter) {
        this.adapter = adapter;
    }

    public ArrayAdapter<Tournament> getAdapter() {
        Log.i("Get Adapter: ", "I am passing the adapter");
        return adapter;
    }

    public List<Tournament> getValues() {
        Log.i("Tournament List ", "I am passing the Tournament List");
        return values;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.tournaments, container, false);
        listView = (ListView) view.findViewById(android.R.id.list);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("ActivityCreated", "I am in the activity created");
        setRetainInstance(true);
        db = new TournamentDataSource(this.getActivity());

        db.open();
        Log.i("Value", "VALUE FOR SEARCH: " + query);
        values = db.getlist(getQuery());
        adapter = new TournamentAdapter(this.getActivity(), values);

        if (adapter.isEmpty())
        {
            Tournament thisTournament = new Tournament();
            thisTournament.setName("No Tournaments Found");
            thisTournament.setAddress("");
            thisTournament.setStartDate("");
            adapter.add(thisTournament);
        }
        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //here we will need to load the details fragment.
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        DetailsFragment detailsFragment = new DetailsFragment();
        Tournament thisTournament = values.get(position);
        Log.i("Values List Item Click:", thisTournament.toString());
        detailsFragment.setTournament(thisTournament);
        Log.i("View", "On list item click sent list view: " + l.toString() + " view: " + v.toString());
        fragmentTransaction.replace(R.id.content_frame, detailsFragment, "DETAILS_FRAGMENT").addToBackStack("details");

        fragmentTransaction.commit();
        Log.i("FragmentList", "Item clicked: " + id);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml

    //Need to add code to display details of item in separate fragment.

    @Override
    public void onResume() {
        db.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        db.close();
        super.onPause();
    }

}



