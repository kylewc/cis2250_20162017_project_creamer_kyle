package com.example.kcreamer.golftournaments;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kyle on 1/25/2017.
 */

public class PlayerAdapter extends ArrayAdapter<Player> {
    private final ArrayList<Player> values;

    static class ViewHolder
    {
        TextView playerName;
        TextView playerTeam;
        TextView playerEmail;
    }

    private final Context context;

    public PlayerAdapter(Context context,ArrayList<Player> data) {
        super(context,R.layout.player_list_item, data);
        this.context = context;
        this.values = data;
    }

    public void updatePlayers(ArrayList<Player> newlist) {
        values.clear();
        values.addAll(newlist);
        this.notifyDataSetChanged();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();

        if(convertView == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.player_list_item, parent, false);

            holder = new ViewHolder();
            holder.playerName = (TextView) convertView.findViewById(R.id.playerName);
            holder.playerTeam = (TextView)convertView.findViewById(R.id.playerTeam);
            holder.playerEmail = (TextView)convertView.findViewById(R.id.playerEmail);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        Player item = getItem(position);
        if (item!= null) {
            holder.playerName.setText(String.format( item.getPlayerName()));
            holder.playerTeam.setText(String.format( item.getPlayerTeam()));
            holder.playerEmail.setText(String.format( item.getPlayerEmail()));
        }
        Player player= values.get(position);

        holder.playerName.setText(player.getPlayerName());
        holder.playerTeam.setText(player.getPlayerTeam());
        holder.playerEmail.setText(player.getPlayerEmail());

        return convertView;
    }


    public void update () {
        notifyDataSetChanged();
    }
}
