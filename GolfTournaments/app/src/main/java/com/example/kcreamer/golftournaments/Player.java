package com.example.kcreamer.golftournaments;

/**
 * Created by Kyle on 1/25/2017.
 */

public class Player {

    private int id;
    private String playerId;
    private String playerName;
    private String playerEmail;
    private String playerTeam;
    private String tournamentID;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getTournamentID() {
        return tournamentID;
    }

    public void setTournamentID(String tournamentID) {
        this.tournamentID = tournamentID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerEmail() {
        return playerEmail;
    }

    public void setPlayerEmail(String playerEmail) {
        this.playerEmail = playerEmail;
    }

    public String getPlayerTeam() {
        return playerTeam;
    }

    public void setPlayerTeam(String playerTeam) {
        this.playerTeam = playerTeam;
    }

    @Override
    public String toString() {
        return "Player name:" + playerName + "\n" +
                "Team:"+ playerTeam + "\n" +
                playerEmail;
    }
}

