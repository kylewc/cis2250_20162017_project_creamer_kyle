package com.example.kcreamer.golftournaments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class SearchableActivity extends  MainActivity {
    TournamentDataSource db;
    private ArrayAdapter<Tournament> adapter;
    private ListView thisList;
    private ArrayList<Tournament> values;
    private Tournament thisTournament;
    private String query;
    private SearchList searchList;
    private DetailsFragment detailsFragment;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        TournamentDataSource db = new TournamentDataSource(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        DetailsFragment detailsFragmentCheck = (DetailsFragment) fm.findFragmentByTag("DETAILS_FRAGMENT");
        if (detailsFragmentCheck == null) {


            // Get the intent, verify the action and get the query
            Intent intent = getIntent();

            if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
                query = intent.getStringExtra(SearchManager.QUERY);
                searchList = new SearchList();
                searchList.setQuery(query);
                ft.replace(R.id.content_frame, searchList, "SEARCH_LIST").addToBackStack("SEARCH_LIST").commit();

            } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                detailsFragment = (DetailsFragment) fm.findFragmentByTag("DETAILS_FRAGMENT");
                if (detailsFragment == null) {
                    List<Tournament> values = db.getAllTournaments();
                    detailsFragment = new DetailsFragment();
                    for (Tournament tournament : values) {
                        if (Integer.toString(tournament.getId()).equals(intent.getExtras().getString(SearchManager.EXTRA_DATA_KEY))) {
                            thisTournament = tournament;
                            detailsFragment.setTournament(thisTournament);
                        } else {
                            Log.i("error", "There was an error locating the tournament");
                        }
                    }
                }
                fm.beginTransaction()
                        .replace(R.id.content_frame, detailsFragment, "DETAILS_FRAGMENT").addToBackStack("details")
                        .commit();
            }
//        } else {
//            if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
//
//                Log.i("Landscape", "I am landscape.");
//                ft.replace(R.id.frameDetails, detailsFragmentCheck, "DETAILS_FRAGMENT").addToBackStack("SEARCH_RESULTS");
//                ft.commit();
//
//                Log.i("Portrait", "I am portrait.");
//                ft.replace(R.id.content_frame, detailsFragment, "DETAILS_FRAGMENT").addToBackStack("SEARCH_RESULTS");
//                ft.commit();
            }
        }

    }






