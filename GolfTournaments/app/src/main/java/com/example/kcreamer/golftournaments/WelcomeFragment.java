package com.example.kcreamer.golftournaments;

import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.lang.reflect.Field;


/**
 * Created by shina on 2017-01-18.
 */

public class WelcomeFragment extends Fragment {
    private static final int NUM_PAGES = 4;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    View myView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myView = inflater.inflate(R.layout.fragment_welcome, container, false);
        // Locate the ViewPager in viewpager_main.xml
        // Set the ViewPagerAdapter into ViewPager
        MyAdapter mCustomPagerAdapter = new MyAdapter(this.getContext());

        ViewPager mViewPager = (ViewPager) myView.findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);

        return myView;

    }


    public static class MyAdapter extends PagerAdapter {
        LayoutInflater inflater;

        private int[] images = {R.drawable.golf1, R.drawable.golf2, R.drawable.golf3, R.drawable.golf4};
        Context mContext;
        LayoutInflater mLayoutInflater;

        public MyAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.fragment_slider, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imgView);
            imageView.setImageResource(images[position]);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

        @Override
        public void onDetach() {
            super.onDetach();

            try {
                Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
                childFragmentManager.setAccessible(true);
                childFragmentManager.set(this, null);

            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }

}


