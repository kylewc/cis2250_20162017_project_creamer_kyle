package com.example.kcreamer.golftournaments;


import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;

public class SuggestionsProvider extends ContentProvider{
    private SQLiteDatabase db;
    private SQLiteQueryBuilder qb;
    private String sqlTables = "tournament";

@Override
public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
        }

@Override
public String getType(Uri uri) {
        return null;
        }

@Override
public Uri insert(Uri uri, ContentValues values) {
        return null;
        }

@Override
public boolean onCreate() {

        return false;
        }
public Cursor query(Uri uri, String[] projection, String selection,
        String[] selectionArgs, String sortOrder) {
    MergeCursor mergeCursor;
    TournamentDataSource db = new TournamentDataSource(getContext());
    if (selectionArgs != null && selectionArgs.length > 0
            && selectionArgs[0].length() > 0) {
        ArrayList<Tournament> tournaments = db.getlist(selectionArgs[0]);

        Cursor c = null;
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{"_id", "suggest_text_1", SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});
        for(Tournament tournament : tournaments)
        {
            matrixCursor.addRow(new Object[]{tournament.getId(), tournament.getName(), tournament.getId()});
        }

        // Merge your existing cursor with the matrixCursor you created.
        mergeCursor = new MergeCursor(new Cursor[]{matrixCursor, c});


            mergeCursor.moveToFirst();

        } else {
            return null;
        }
        return mergeCursor;
    }



        @Override
        public int update(Uri uri, ContentValues values, String selection,
                          String[] selectionArgs) {

        return 0;
        }

        }

