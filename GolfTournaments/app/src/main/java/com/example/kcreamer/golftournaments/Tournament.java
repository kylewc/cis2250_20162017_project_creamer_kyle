package com.example.kcreamer.golftournaments;

/**
 * Created by kcreamer on 1/20/2017.
 */

public class Tournament {

    private int id;
    private String name;
    private String startDate;
    private String endDate;
    private String address;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Tournament Name: " + name + "\n" +
                " on "+ startDate + "\n" +
                " at " + address;
    }
}

